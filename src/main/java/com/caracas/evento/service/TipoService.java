package com.caracas.evento.service;

import com.caracas.evento.models.Tipo;
import com.caracas.evento.models.TiposResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TipoService {

    ResponseEntity<Void> postTipoEvento(Tipo body);

    ResponseEntity<List<TiposResponse>> getTipoEvento();

}
