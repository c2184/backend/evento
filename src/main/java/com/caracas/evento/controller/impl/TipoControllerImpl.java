package com.caracas.evento.controller.impl;

import com.caracas.evento.controller.EventoController;
import com.caracas.evento.models.Tipo;
import com.caracas.evento.models.TiposResponse;
import com.caracas.evento.service.TipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TipoControllerImpl implements EventoController{

    @Autowired
    TipoService tipoService;

    @Override
    public ResponseEntity<Void> postTipoEvento(Tipo body){
        return tipoService.postTipoEvento(body);
    }

    @Override
    public ResponseEntity<List<TiposResponse>> getTipoEvento() {
        return tipoService.getTipoEvento();
    }
}
