package com.caracas.evento.controller;

import com.caracas.evento.models.*;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
@Api(value = "evento", description = "the evento API")
public interface EventoController {

    @ApiOperation(value = "", nickname = "postTipoEvento", notes = "Cadastra um novo tipo de evento", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Tipo",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Novo `tipo` de evento criado com sucesso"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/tipo",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> postTipoEvento(@ApiParam(value = "") @Valid @RequestBody Tipo body) {
        return null;
    }

    @ApiOperation(value = "", nickname = "getTipoEvento", notes = "Retorna todos os tipos cadastrados", response = TiposResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Tipo",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A requisição retornou com sucesso", response = TiposResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/tipo",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<TiposResponse>> getTipoEvento() {
        return null;
    }

    @ApiOperation(value = "", nickname = "postCategoriaEvento", notes = "Cadastra uma nova categoria de evento", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Categoria",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Nova `categoria` de evento criada com sucesso"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/categoria",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> postCategoriaEvento(@ApiParam(value = "") @Valid @RequestBody Categoria body) {
        return null;
    }

    @ApiOperation(value = "", nickname = "getCategoriaEvento", notes = "Retorna todas as categorias cadastradas", response = CategoriasResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Categoria",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A requisição retornou com sucesso", response = CategoriasResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/categoria",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<CategoriasResponse>> getCategoriaEvento() {
        return null;
    }

    @ApiOperation(value = "", nickname = "postAtividadeEvento", notes = "Cadastra uma nova atividade de evento", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Atividade",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Nova `atividade` criada com sucesso"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/atividade",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> postAtividadeEvento(@ApiParam(value = "") @Valid @RequestBody Atividade body) {
        return null;
    }

    @ApiOperation(value = "", nickname = "getAtividadeEvento", notes = "Retorna todas as atividades cadastradas", response = AtividadesResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Atividade",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A requisição retornou com sucesso", response = AtividadesResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/atividade",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<AtividadesResponse>> getAtividadeEvento() {
        return null;
    }

    @ApiOperation(value = "", nickname = "postEvento", notes = "Cadastra um novo Evento", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Evento",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Novo `evento` criado com sucesso"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/evento",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    default ResponseEntity<Void> postEvento(@ApiParam(value = "") @Valid @RequestBody Evento body) {
        return null;
    }

    @ApiOperation(value = "", nickname = "getEvento", notes = "Retorna todos os eventos cadastrados", response = EventosResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"Evento",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A requisição retornou com sucesso", response = EventosResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbiden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/evento/v1/evento",
            produces = {"application/json"},
            method = RequestMethod.GET)
    default ResponseEntity<List<EventosResponse>> getEvento() {
        return null;
    }

}
