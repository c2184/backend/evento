package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

/**
 * EventosResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class EventosResponse extends Evento  {
  @JsonProperty("idEvento")
  private Integer idEvento = null;

  public EventosResponse idEvento(Integer idEvento) {
    this.idEvento = idEvento;
    return this;
  }

  /**
   * Identificador do evento
   * minimum: 1
   * maximum: 99999999999
   * @return idEvento
  **/
  @ApiModelProperty(example = "320", value = "Identificador do evento")
  
  @Min(1) @Max(999999999)
  public Integer getIdEvento() {
    return idEvento;
  }

  public void setIdEvento(Integer idEvento) {
    this.idEvento = idEvento;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventosResponse eventosResponse = (EventosResponse) o;
    return Objects.equals(this.idEvento, eventosResponse.idEvento) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idEvento, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventosResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    idEvento: ").append(toIndentedString(idEvento)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
