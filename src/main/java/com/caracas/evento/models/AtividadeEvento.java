package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * AtividadeEvento
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class AtividadeEvento extends Atividade  {
  @JsonProperty("quantidadeVagas")
  private Integer quantidadeVagas = null;

  public AtividadeEvento quantidadeVagas(Integer quantidadeVagas) {
    this.quantidadeVagas = quantidadeVagas;
    return this;
  }

  /**
   * Limites de vagas por atividade
   * @return quantidadeVagas
  **/
  @ApiModelProperty(example = "30", required = true, value = "Limites de vagas por atividade")
      @NotNull

    public Integer getQuantidadeVagas() {
    return quantidadeVagas;
  }

  public void setQuantidadeVagas(Integer quantidadeVagas) {
    this.quantidadeVagas = quantidadeVagas;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AtividadeEvento atividadeEvento = (AtividadeEvento) o;
    return Objects.equals(this.quantidadeVagas, atividadeEvento.quantidadeVagas) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(quantidadeVagas, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AtividadeEvento {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    quantidadeVagas: ").append(toIndentedString(quantidadeVagas)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
