package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Tipo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class Tipo   {
  @JsonProperty("nomeTipo")
  private String nomeTipo = null;

  @JsonProperty("descricaoTipo")
  private String descricaoTipo = null;

  @JsonProperty("statusTipo")
  private Boolean statusTipo = null;

  public Tipo nomeTipo(String nomeTipo) {
    this.nomeTipo = nomeTipo;
    return this;
  }

  /**
   * Nome do Tipo do evento
   * @return nomeTipo
  **/
  @ApiModelProperty(example = "Acadêmico", required = true, value = "Nome do Tipo do evento")
      @NotNull

    public String getNomeTipo() {
    return nomeTipo;
  }

  public void setNomeTipo(String nomeTipo) {
    this.nomeTipo = nomeTipo;
  }

  public Tipo descricaoTipo(String descricaoTipo) {
    this.descricaoTipo = descricaoTipo;
    return this;
  }

  /**
   * Breve descrição do Tipo
   * @return descricaoTipo
  **/
  @ApiModelProperty(example = "Evento de nicho acadêmico", required = true, value = "Breve descrição do Tipo")
      @NotNull

    public String getDescricaoTipo() {
    return descricaoTipo;
  }

  public void setDescricaoTipo(String descricaoTipo) {
    this.descricaoTipo = descricaoTipo;
  }

  public Tipo statusTipo(Boolean statusTipo) {
    this.statusTipo = statusTipo;
    return this;
  }

  /**
   * Status do Tipo, se esta ativo ou não
   * @return statusTipo
  **/
  @ApiModelProperty(example = "true", required = true, value = "Status do Tipo, se esta ativo ou não")
      @NotNull

    public Boolean isStatusTipo() {
    return statusTipo;
  }

  public void setStatusTipo(Boolean statusTipo) {
    this.statusTipo = statusTipo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tipo tipo = (Tipo) o;
    return Objects.equals(this.nomeTipo, tipo.nomeTipo) &&
        Objects.equals(this.descricaoTipo, tipo.descricaoTipo) &&
        Objects.equals(this.statusTipo, tipo.statusTipo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nomeTipo, descricaoTipo, statusTipo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Tipo {\n");
    
    sb.append("    nomeTipo: ").append(toIndentedString(nomeTipo)).append("\n");
    sb.append("    descricaoTipo: ").append(toIndentedString(descricaoTipo)).append("\n");
    sb.append("    statusTipo: ").append(toIndentedString(statusTipo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
