package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Evento
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class Evento   {
  @JsonProperty("nomeEvento")
  private String nomeEvento = null;

  @JsonProperty("descricaoEvento")
  private String descricaoEvento = null;

  @JsonProperty("dataInicio")
  private LocalDate dataInicio = null;

  @JsonProperty("dataFim")
  private LocalDate dataFim = null;

  @JsonProperty("tipoEvento")
  private String tipoEvento = null;

  @JsonProperty("categoriaEvento")
  private String categoriaEvento = null;

  @JsonProperty("atividadesEvento")
  @Valid
  private List<AtividadeEvento> atividadesEvento = new ArrayList<AtividadeEvento>();

  @JsonProperty("statusEvento")
  private Boolean statusEvento = null;

  public Evento nomeEvento(String nomeEvento) {
    this.nomeEvento = nomeEvento;
    return this;
  }

  /**
   * Nome do Evento
   * @return nomeEvento
  **/
  @ApiModelProperty(example = "SIMGETEC", required = true, value = "Nome do Evento")
      @NotNull

    public String getNomeEvento() {
    return nomeEvento;
  }

  public void setNomeEvento(String nomeEvento) {
    this.nomeEvento = nomeEvento;
  }

  public Evento descricaoEvento(String descricaoEvento) {
    this.descricaoEvento = descricaoEvento;
    return this;
  }

  /**
   * Breve descrição do Evento
   * @return descricaoEvento
  **/
  @ApiModelProperty(example = "Simposio de Gestão e Tecnologia da FATEC Carapicuiba", required = true, value = "Breve descrição do Evento")
      @NotNull

    public String getDescricaoEvento() {
    return descricaoEvento;
  }

  public void setDescricaoEvento(String descricaoEvento) {
    this.descricaoEvento = descricaoEvento;
  }

  public Evento dataInicio(LocalDate dataInicio) {
    this.dataInicio = dataInicio;
    return this;
  }

  /**
   * Data que inicia o evento
   * @return dataInicio
  **/
  @ApiModelProperty(example = "Mon Dec 06 21:00:00 BRT 2021", required = true, value = "Data que inicia o evento")
      @NotNull

    @Valid
    public LocalDate getDataInicio() {
    return dataInicio;
  }

  public void setDataInicio(LocalDate dataInicio) {
    this.dataInicio = dataInicio;
  }

  public Evento dataFim(LocalDate dataFim) {
    this.dataFim = dataFim;
    return this;
  }

  /**
   * Data que encerra o evento
   * @return dataFim
  **/
  @ApiModelProperty(example = "Thu Dec 09 21:00:00 BRT 2021", required = true, value = "Data que encerra o evento")
      @NotNull

    @Valid
    public LocalDate getDataFim() {
    return dataFim;
  }

  public void setDataFim(LocalDate dataFim) {
    this.dataFim = dataFim;
  }

  public Evento tipoEvento(String tipoEvento) {
    this.tipoEvento = tipoEvento;
    return this;
  }

  /**
   * Tipo do evento
   * @return tipoEvento
  **/
  @ApiModelProperty(example = "Acadêmico", required = true, value = "Tipo do evento")
      @NotNull

    public String getTipoEvento() {
    return tipoEvento;
  }

  public void setTipoEvento(String tipoEvento) {
    this.tipoEvento = tipoEvento;
  }

  public Evento categoriaEvento(String categoriaEvento) {
    this.categoriaEvento = categoriaEvento;
    return this;
  }

  /**
   * Categoria do evento
   * @return categoriaEvento
  **/
  @ApiModelProperty(example = "Simpósio", required = true, value = "Categoria do evento")
      @NotNull

    public String getCategoriaEvento() {
    return categoriaEvento;
  }

  public void setCategoriaEvento(String categoriaEvento) {
    this.categoriaEvento = categoriaEvento;
  }

  public Evento atividadesEvento(List<AtividadeEvento> atividadesEvento) {
    this.atividadesEvento = atividadesEvento;
    return this;
  }

  public Evento addAtividadesEventoItem(AtividadeEvento atividadesEventoItem) {
    this.atividadesEvento.add(atividadesEventoItem);
    return this;
  }

  /**
   * Atividades do evento
   * @return atividadesEvento
  **/
  @ApiModelProperty(required = true, value = "Atividades do evento")
      @NotNull
    @Valid
    public List<AtividadeEvento> getAtividadesEvento() {
    return atividadesEvento;
  }

  public void setAtividadesEvento(List<AtividadeEvento> atividadesEvento) {
    this.atividadesEvento = atividadesEvento;
  }

  public Evento statusEvento(Boolean statusEvento) {
    this.statusEvento = statusEvento;
    return this;
  }

  /**
   * Status do evento, se esta ativo ou não
   * @return statusEvento
  **/
  @ApiModelProperty(example = "true", required = true, value = "Status do evento, se esta ativo ou não")
      @NotNull

    public Boolean isStatusEvento() {
    return statusEvento;
  }

  public void setStatusEvento(Boolean statusEvento) {
    this.statusEvento = statusEvento;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Evento evento = (Evento) o;
    return Objects.equals(this.nomeEvento, evento.nomeEvento) &&
        Objects.equals(this.descricaoEvento, evento.descricaoEvento) &&
        Objects.equals(this.dataInicio, evento.dataInicio) &&
        Objects.equals(this.dataFim, evento.dataFim) &&
        Objects.equals(this.tipoEvento, evento.tipoEvento) &&
        Objects.equals(this.categoriaEvento, evento.categoriaEvento) &&
        Objects.equals(this.atividadesEvento, evento.atividadesEvento) &&
        Objects.equals(this.statusEvento, evento.statusEvento);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nomeEvento, descricaoEvento, dataInicio, dataFim, tipoEvento, categoriaEvento, atividadesEvento, statusEvento);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Evento {\n");
    
    sb.append("    nomeEvento: ").append(toIndentedString(nomeEvento)).append("\n");
    sb.append("    descricaoEvento: ").append(toIndentedString(descricaoEvento)).append("\n");
    sb.append("    dataInicio: ").append(toIndentedString(dataInicio)).append("\n");
    sb.append("    dataFim: ").append(toIndentedString(dataFim)).append("\n");
    sb.append("    tipoEvento: ").append(toIndentedString(tipoEvento)).append("\n");
    sb.append("    categoriaEvento: ").append(toIndentedString(categoriaEvento)).append("\n");
    sb.append("    atividadesEvento: ").append(toIndentedString(atividadesEvento)).append("\n");
    sb.append("    statusEvento: ").append(toIndentedString(statusEvento)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
