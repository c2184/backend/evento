package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

/**
 * TiposResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class TiposResponse extends Tipo  {
  @JsonProperty("idTipo")
  private Integer idTipo = null;

  public TiposResponse idTipo(Integer idTipo) {
    this.idTipo = idTipo;
    return this;
  }

  /**
   * Identificador do tipo
   * minimum: 1
   * maximum: 99999999999
   * @return idTipo
  **/
  @ApiModelProperty(example = "14", value = "Identificador do tipo")
  
  @Min(1) @Max(999999999)
  public Integer getIdTipo() {
    return idTipo;
  }

  public void setIdTipo(Integer idTipo) {
    this.idTipo = idTipo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TiposResponse tiposResponse = (TiposResponse) o;
    return Objects.equals(this.idTipo, tiposResponse.idTipo) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idTipo, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TiposResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    idTipo: ").append(toIndentedString(idTipo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
