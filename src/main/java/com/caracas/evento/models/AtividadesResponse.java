package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

/**
 * AtividadesResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class AtividadesResponse extends Atividade  {
  @JsonProperty("idAtividade")
  private Integer idAtividade = null;

  public AtividadesResponse idAtividade(Integer idAtividade) {
    this.idAtividade = idAtividade;
    return this;
  }

  /**
   * Identificador da atividade
   * minimum: 1
   * maximum: 99999999999
   * @return idAtividade
  **/
  @ApiModelProperty(example = "55", value = "Identificador da atividade")
  
  @Min(1) @Max(999999999)
  public Integer getIdAtividade() {
    return idAtividade;
  }

  public void setIdAtividade(Integer idAtividade) {
    this.idAtividade = idAtividade;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AtividadesResponse atividadesResponse = (AtividadesResponse) o;
    return Objects.equals(this.idAtividade, atividadesResponse.idAtividade) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idAtividade, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AtividadesResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    idAtividade: ").append(toIndentedString(idAtividade)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
