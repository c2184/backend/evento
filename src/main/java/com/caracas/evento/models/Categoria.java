package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Categoria
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class Categoria   {
  @JsonProperty("nomeCategoria")
  private String nomeCategoria = null;

  @JsonProperty("descricaoCategoria")
  private String descricaoCategoria = null;

  @JsonProperty("statusCategoria")
  private Boolean statusCategoria = null;

  public Categoria nomeCategoria(String nomeCategoria) {
    this.nomeCategoria = nomeCategoria;
    return this;
  }

  /**
   * Nome da categoria do evento
   * @return nomeCategoria
  **/
  @ApiModelProperty(example = "Congresso", required = true, value = "Nome da categoria do evento")
      @NotNull

    public String getNomeCategoria() {
    return nomeCategoria;
  }

  public void setNomeCategoria(String nomeCategoria) {
    this.nomeCategoria = nomeCategoria;
  }

  public Categoria descricaoCategoria(String descricaoCategoria) {
    this.descricaoCategoria = descricaoCategoria;
    return this;
  }

  /**
   * Breve descrição da categoria
   * @return descricaoCategoria
  **/
  @ApiModelProperty(example = "Evento para apresentação de portifolio", required = true, value = "Breve descrição da categoria")
      @NotNull

    public String getDescricaoCategoria() {
    return descricaoCategoria;
  }

  public void setDescricaoCategoria(String descricaoCategoria) {
    this.descricaoCategoria = descricaoCategoria;
  }

  public Categoria statusCategoria(Boolean statusCategoria) {
    this.statusCategoria = statusCategoria;
    return this;
  }

  /**
   * Status da categoria, se esta ativa ou não
   * @return statusCategoria
  **/
  @ApiModelProperty(example = "true", required = true, value = "Status da categoria, se esta ativa ou não")
      @NotNull

    public Boolean isStatusCategoria() {
    return statusCategoria;
  }

  public void setStatusCategoria(Boolean statusCategoria) {
    this.statusCategoria = statusCategoria;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Categoria categoria = (Categoria) o;
    return Objects.equals(this.nomeCategoria, categoria.nomeCategoria) &&
        Objects.equals(this.descricaoCategoria, categoria.descricaoCategoria) &&
        Objects.equals(this.statusCategoria, categoria.statusCategoria);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nomeCategoria, descricaoCategoria, statusCategoria);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Categoria {\n");
    
    sb.append("    nomeCategoria: ").append(toIndentedString(nomeCategoria)).append("\n");
    sb.append("    descricaoCategoria: ").append(toIndentedString(descricaoCategoria)).append("\n");
    sb.append("    statusCategoria: ").append(toIndentedString(statusCategoria)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
