package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

/**
 * CategoriasResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class CategoriasResponse extends Categoria  {
  @JsonProperty("idCategoria")
  private Integer idCategoria = null;

  public CategoriasResponse idCategoria(Integer idCategoria) {
    this.idCategoria = idCategoria;
    return this;
  }

  /**
   * Identificador da Categoria
   * minimum: 1
   * maximum: 99999999999
   * @return idCategoria
  **/
  @ApiModelProperty(example = "35", value = "Identificador da Categoria")
  
  @Min(1) @Max(999999999)
  public Integer getIdCategoria() {
    return idCategoria;
  }

  public void setIdCategoria(Integer idCategoria) {
    this.idCategoria = idCategoria;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CategoriasResponse categoriasResponse = (CategoriasResponse) o;
    return Objects.equals(this.idCategoria, categoriasResponse.idCategoria) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idCategoria, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CategoriasResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    idCategoria: ").append(toIndentedString(idCategoria)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
