package com.caracas.evento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Atividade
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-10-05T23:51:18.251363900-03:00[America/Sao_Paulo]")
public class Atividade   {
  @JsonProperty("nomeAtividade")
  private String nomeAtividade = null;

  @JsonProperty("statusAtividade")
  private Boolean statusAtividade = null;

  public Atividade nomeAtividade(String nomeAtividade) {
    this.nomeAtividade = nomeAtividade;
    return this;
  }

  /**
   * Nome da atividade em um evento
   * @return nomeAtividade
  **/
  @ApiModelProperty(example = "Workshop", required = true, value = "Nome da atividade em um evento")
      @NotNull

  public String getNomeAtividade() {
    return nomeAtividade;
  }

  public void setNomeAtividade(String nomeAtividade) {
    this.nomeAtividade = nomeAtividade;
  }

  public Atividade statusAtividade(Boolean statusAtividade) {
    this.statusAtividade = statusAtividade;
    return this;
  }

  /**
   * Status da atividade, se esta ativa ou não
   * @return statusAtividade
  **/
  @ApiModelProperty(example = "true", required = true, value = "Status da atividade, se esta ativa ou não")
      @NotNull

  public Boolean isStatusAtividade() {
    return statusAtividade;
  }

  public void setStatusAtividade(Boolean statusAtividade) {
    this.statusAtividade = statusAtividade;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Atividade atividade = (Atividade) o;
    return Objects.equals(this.nomeAtividade, atividade.nomeAtividade) &&
        Objects.equals(this.statusAtividade, atividade.statusAtividade);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nomeAtividade, statusAtividade);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Atividade {\n");
    
    sb.append("    nomeAtividade: ").append(toIndentedString(nomeAtividade)).append("\n");
    sb.append("    statusAtividade: ").append(toIndentedString(statusAtividade)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
